
import string 
import numpy as np 
from nltk import word_tokenize,sent_tokenize
from nltk.corpus import stopwords 
import os
import time
import logging
from gensim.models import Word2Vec
#import create_df


feedback_list = []
feedback_tokens = []
class Mysentences():
    def __iter__(self):
        for feedback in feedback_list:
            yield feedback
            
def word2vec(content,path):
    punctuations = set(string.punctuation)
    stop_words = set(stopwords.words('english'))
    
    for sentence in content:
        list_words = sentence.split()
        for i in list_words:
            if(i[-1]=='.'or i[-1]==',' or i[-1]=='$' or i[-1]==')'):
                word = i[0:-1]
                #print(word)
                if((word not in stop_words) and not(word.isdigit())):
                        feedback_tokens.append(word.lower())
            else:
                word = i
                if((word not in stop_words) and not(word.isdigit())):
                        feedback_tokens.append(word.lower())
        feedback_list.append(feedback_tokens)
        
    #len(feedback_list)
    
    #directory= '/home/ubuntu/trip_advisor/Entity_Engine/New_word2vec'
    directory= path
    if not os.path.exists(directory):
        os.makedirs(directory)
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    start_time = time.time()
    training_sentences = Mysentences()
    #gensim.models.word2vec.LineSentence()
    # instantiating the word2vec model and calling the iterator class object as one element to train
    # parameters 
    # min_count --> ignoring words which are less than this frequency
    # size --> size of the output vector
    # window --> maximum distance between the current and predicted words within a sentence
    # workers -- > number of worker threads to train the model(multicore machines help)
    # uses heirarchical sampling for computing the output
    # compute loss --> computes and stores the loss computed in during training
    model = Word2Vec(training_sentences , min_count = 5,iter = 5,size = 300 ,window = 10 ,workers=150,hs = 1,compute_loss = True)
    model.save(directory+'/Word2Vec_version1')
    #print('word2vec Model build successfully!!!!!')
   

            

