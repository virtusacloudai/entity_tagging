#preprocessing , word2vec and clustering 

import sys
import pandas as pd
import gzip
import os
from pandas.io.json import json_normalize
import json
import word2vec_build
import time
from gensim.models import Word2Vec
from flask import Flask, request , jsonify
import requests

def parse(path):
    g = gzip.open(path, 'rb')
    for l in g:
        yield eval(l)

def getDF(path):
    print('Loading Dataframe .........')
    i = 0
    df = {}
    inputFilepath = path
    filename_w_ext = os.path.basename(inputFilepath)
    filename, file_extension = os.path.splitext(filename_w_ext)
    if(file_extension == ".gz"):
        for d in parse(path):
            df[i] = d
            i += 1
        return pd.DataFrame.from_dict(df, orient='index')
    elif(file_extension == ".csv"):
         return pd.read_csv(path,encoding = "ISO-8859-1")
    elif(file_extension == ".xlsx"):
        return pd.read_excel(path)
    elif(file_extension == ".json"):
        with open(path) as train_file:
            dict_train = json.load(train_file)
        #converting json dataset from dictionary to dataframe
        train = pd.DataFrame.from_dict(json_normalize(dict_train), orient='columns')
        return train
    else:
         print('File Format is not matched')
            
app = Flask(__name__)
@app.route("/word2vec",methods = ['POST'])
def model_build():
    if request.method == 'POST':
        #file_path="/home/ubuntu/trip_advisor/Entity_Engine/Create_Dataset/reviews_Apps_for_Android_5.json.gz"# path to dataset
        #path="/home/ubuntu/trip_advisor/Entity_Engine/WORD2VEC"# path to save word2vec
        #print("welcome")
        content = request.get_json()
        file_path=content['file_path']
        path=content['path']
        df=getDF(file_path)
        #print("dataframe build done")
        column_name = df.columns[0]
        content=df[column_name]
        print('............Building word2vec.............')
        word2vec_build.word2vec(content,path)
        return 'model build succesfully',200
        
    else:
        return 'Internal Server Error', 500
     #   model=Word2Vec.load('/home/ubuntu/trip_advisor/Entity_Engine/WORD2VEC/Word2Vec_version1')
    #     start = time.time()
    #     X = model[model.wv.vocab]
    #     print('...................Running Gap Statistics.................')
    #     k, gapdf = gap.optimalK(X, nrefs=3, maxClusters=20)
    #     print ('Optimal k is: ', k)
    #     print('Total time to find clusters :',time.time()-start)
        
    #     clustering.build_cluster(model,k,X)
    #     print(".............clustering Done...........")
    
    
if __name__ == "__main__": 
    app.run(host="0.0.0.0",port=1200, debug=False)    
